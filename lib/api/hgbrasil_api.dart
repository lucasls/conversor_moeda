import 'package:conversor_moeda/model/hgbrasil/hg_brasil.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

const request = "https://api.hgbrasil.com/finance?format=json&key=53bd0782";

class HgBrasilApi {
  Future<HgBrasil> getData() async {
    http.Response response = await http.get(request);
    final res = json.decode(response.body);
    final hgBrasil = HgBrasil.fromJson(res);
    return hgBrasil;
  }
}
